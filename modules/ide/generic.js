'use strict';

const ScrollObjectEditor = require('../../lib/ScrollObjectEditor');

class Generic extends ScrollObjectEditor {
    constructor(...args) {
        super(...args);
        this.workspace = this.editor.workspace;
        this.preview_obj = this.workspace.objects.document[0];
        this.setup_events();
    }

    setup_events() {
        this.on('save', (event, new_value) => {
            // this.scrollobj.meta.contents = new_value;
            this.scrollobj.workspace.reload(this.scrollobj, new_value);
            this.update();
        });
    }

    get_preview() {
        const html = this.preview_obj.actions.render('editor');
        const css = this.preview_obj.actions.rendercss('editor');
        return {html, css};
    }

    get_opts() {
        const {html, css} = this.get_preview();
        return {
            info: this.scrollobj.info,
            contents: this.scrollobj.meta.contents,
            tag_name: this.scrollobj.name,
            preview_html: html,
            preview_css: css,
        };
    }

    get tagname() {
        // Name of the channel this should use
        return 'generic';
    }
}

module.exports = Generic;
