<generic>
		<style scoped>
				:scope {
						text-align: left;
				}
				textarea {
						height: 70vh;
						font-size: 10.5pt;
				}
		</style>

		<raw-style css={opts.preview_css} />

		<div class="left-pane z-depth-3 animated slideInLeft">
				<div class="left-pane-title">
						<h5>{opts.tag_name}</h5>
						<!--
						<button onclick={toggle_workspace} class="waves-effect waves-teal btn-flat left-pane-close-button">
								<i class="large mdi-navigation-arrow-back"></i>
						</button>
						-->
				</div>
				<div class="left-pane-contents">
						<textarea onkeyup={save} onchange={save}>{opts.contents}</textarea>
				</div>
		</div>
		<div class="right-pane left-pane-visible">
				<div style="width: 300px; display: inline-block">
						<div class="input-field">
								<select>
										<option value="1">Document</option>
								</select>
								<label>Preview</label>
						</div>
				</div>
				<div id="preview_pane">
						<raw html={opts.preview_html} />
				</div>
		</div>
		<script>
				'use strict';
				const limit = require('../../static/js/limit');
				this.on('updated', () => {
						console.log('prepping select');
						$('select').material_select();
				});

				save(ev) {
						limit.debounce(function () {
								const value = ev.target.value;
								this.opts.send('save', value);
						}, 100, this);
						//const value = ev.target.value;
						//this.opts.send('save', value);
				}
		</script>
</generic>

<!--
<style>
		.schema-block {
				font-family: monospace;
				border-top: 2px #ddd solid;
		}

		.schema-block .schema-contents {
				background-color: #ddd;
				display: block;
		}

		.schema-block .schema-block-title h4 {
				padding: 0;
				margin: 0;
				font-size: 16pt;
				font-weight: bold;
				text-align: right;
		}

		.schema-block .schema-block-title label {
				font-weight: bold;
				text-align: right;
				display: block;
		}

		.schema-block-input input {
		}
</style>

<div class="schema-block row">
		<div class="col s2 schema-block-title">
				<h4>markdown</h4>
		</div>
		<div class="col s10 schema-contents">
				<div class="col s3 schema-block-title">
						<label>markdown</label>
				</div>
				<div class="col s9 schema-block-input">
						<input value={opts.value} name={opts.name} id={opts.name} type="text" />
				</div>
		</div>
</div>

<mat-input-field name='markdown' value='{opts.info.markdown.markdown}'/>
<mat-input-field name='contains' value='{opts.info.markdown.contains}'/>
		Markdown
</mat-input-field>
<mat-input-field name='help' value='{opts.info.tag.help}'>
		Help
</mat-input-field>

<mat-input-field name='contains' value='{opts.info.markdown.contains}'>
		Contains
</mat-input-field>

<h5>Editor</h5>

<mat-input-field name='keycode' value='{opts.info.editor.keycode}'>
		Keycode (Ctrl +)
</mat-input-field>

<mat-input-field name='short_label' value='{opts.info.editor.short_label}'>
		Short label
</mat-input-field>

<h5>Markdown</h5>

<h5>Style</h5>
-->

