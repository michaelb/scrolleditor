'use strict';

const ScrollObjectEditor = require('../../lib/ScrollObjectEditor');

function capitalize(str) {
    return str.slice(0, 1).toUpperCase() + str.slice(1);
}

class Workspace extends ScrollObjectEditor {
    get tagname() {
        // Name of the .tag and channel this should use
        return 'workspace';
    }

    constructor(...args) {
        super(...args);
        this.setup_events();

        this.groups = [];
        const make_group = typename =>
            ({label: capitalize(typename), typename: typename,
                collapsed: true, objects: []});

        this.groups = [
            make_group('document'),
            make_group('tag'),
            make_group('image'),
            //make_group('style'),
        ];

        this.pane_visible = true;
    }

    activate(path) {
        this.set_active(obj => obj.path === path);
        const activated = this.scrollobj.objects.get(`path:${path}`);
        this.editor.mount(null, activated, '#editor_pane');
        this.update();
    }

    setup_events(match) {
        this.on('activate', (event, item_path) => {
            this.activate(item_path);
        });

        this.on('toggle', (event, typename) => {
            const group = this.get_group(typename);
            if (!group) {
                throw new Error('could not find group ' + typename);
            }
            group.collapsed = !group.collapsed;
            this.update();
        });

        this.on('ready', (event, payload) => {
            this.activate('document/document.md');
        });

        this.on('toggle_workspace', (event, payload) => {
            this.pane_visible = !this.pane_visible;
            this.update();
        });
    }

    set_active(match) {
        this.objs = [];
        for (const obj of this.scrollobj.objects) {
            this.objs.push({
                path: obj.path,
                active: match(obj),
                typename: obj.typename,
                title: capitalize(obj.name || obj.typename || ''),
            });
        }

        // clear objects since we just made them anew
        for (const group of this.groups) {
            group.objects = [];
        }

        // (re)-assign
        for (const obj of this.objs) {
            const group = this.get_group(obj.typename);
            if (group) {
                group.objects.push(obj);
                if (obj.active) {
                    // decollapse if active
                    group.collapsed = false;
                }
            }
        }
    }

    get_group(typename) {
        return this.groups.filter(group => group.typename === typename)[0];
    }

    get_opts() {
        return {
            pane_visible: this.pane_visible,
            groups: this.groups,
            objects: this.objs,
        };
    }
}

module.exports = Workspace;
