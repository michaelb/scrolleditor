'use strict';

const ScrollObjectEditor = require('../../lib/ScrollObjectEditor');

class Image extends ScrollObjectEditor {
    constructor(...args) {
        super(...args);
        this.setup_events();
        this.update();
    }

    setup_events() {
        /*
        this.on('save', new_value => {
            this.scrollobj.meta.contents = new_value;
        });
        */
    }

    get_opts() {
        return {
            fullpath: this.scrollobj.fullpath,
        };
    }

    get tagname() {
        // Name of the channel this should use
        return 'image';
    }
}

module.exports = Image;
