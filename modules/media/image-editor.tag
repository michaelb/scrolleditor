<image-editor>
		<div>
				<style scoped>
						#image_editor_wrapper {
								background-color: gray;
								background-image: linear-gradient(90deg, transparent 50%, rgba(255,255,255,.5) 50%);
								background-size: 50px 50px;
								min-width: 100%;
								min-height: 100vh;
								text-align: center;
								padding-top: 100px;
						}
						#image_editor {
								display: inline-block;
						}
						#image_editor img,
						#image_editor canvas {
								display: inline-block;
						}
				</style>
				<div id="image_editor_wrapper">
						<div id="image_editor">
								<!--<img id="target_image" class="animated zoomIn" src='file://{opts.fullpath}' />-->
								<img id="target_image" src='file://{opts.fullpath}' />
						</div>
				</div>

				<script>
						this.on('updated', () => {
								new Darkroom('#target_image');
						});
				</script>
		</div>
</image-editor>
