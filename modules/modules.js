module.exports = {
    types: {
        scrollworkspace: {
            path: 'workspace/workspace',
            tagname: 'workspace',
        },
        document: {
            path: 'document/document',
            tagname: 'document',
        },
        tag: {
            path: 'ide/generic',
            tagname: 'generic',
        },
        // NOTE: 'image' is reserved for <img /> tags, riot / browser issue
        image: {
            path: 'media/image-editor',
            tagname: 'image-editor',
        },
    },
};
