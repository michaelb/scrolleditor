'use strict';

const ScrollObjectEditor = require('../../lib/ScrollObjectEditor');
const cheerio = require('cheerio');
const lodash = require('lodash');

let _uuid = 10000;
function uuid() {
    _uuid++;
    return _uuid;
}

const NORMAL = 0;
const EDITING = 1;

class Document extends ScrollObjectEditor {
    constructor(...args) {
        super(...args);
        this.setup_events()
        this.original_rendered = this.scrollobj.actions.render('editor');
        this.blocks = [];
        this.blocks_by_id = {};
        this.state = NORMAL;
        this.editing = null;

        const $ = cheerio.load(this.original_rendered);
        const $bks = $('bk');
        $bks.each((i, rendered) => {
            const block_info = this.new_block_info($, rendered);
            this.blocks.push(block_info);
        });
    }

    create_block(source) {
        const html = this.scrollobj.editor_render(source);
        const $ = cheerio.load(html);
        return {$, html};
    }

    create_block_info(source) {
        const {$, html} = this.create_block(source);
        return this.new_block_info($, $('bk')[0], source);
    }

    new_block_info($, rendered, source = null) {
        const $rendered = $(rendered);
        const id = uuid();
        source = source || $rendered.attr('data');
        $rendered.attr('uuid', id);
        const class_name = $rendered.attr('class');
        const tag = this.scrollobj.get_tag(class_name);
        let options = {};
        if (tag && tag.containment) {
            // autosplit: tag.info.is_block_container,
            options.valid_tags = tag.containment.tags
                .filter(tag => tag.info.editor)
                .map(tag => ({
                    keycode: tag.info.editor.keycode,
                    short_label: tag.info.editor.short_label,
                    markup: tag.info.markdown.markdown,
                    name: tag.info && tag.info.tag.name,
                }));
        }
        $rendered.removeAttr('data');

        const block_info = {
            preview: $.html(rendered),
            editing: false,
            options,
            class_name,
            id,
            source,
        };

        this.blocks_by_id[id] = block_info;
        return block_info;
    }

    /*
     * Inserts given block info in the array after the occurrence of the block given by uuid
     */
    insert_after(uuid, block_info) {
        if (uuid === null) {
            // insert at the beginning
            this.blocks.unshift(block_info);
            return;
        }

        const index = this.blocks.findIndex(
            block => block.id.toString() === uuid.toString())
        if (index === -1) {
            throw new Error('could not find uuid ' + uuid);
        }
        this.blocks.splice(index + 1, 0, block_info);
    }

    update_block_source(uuid, content) {
        const block_info = this.blocks_by_id[uuid];
        if (!block_info) { throw new Error('could not find block ' + uuid); }
        block_info.source = content;
        block_info.uuid = uuid;

        // Render new HTML source, attach UUID
        const {$, new_html} = this.create_block(content);
        $('bk').attr('uuid', uuid);
        block_info.preview = $.html();
    }

    normal() {
        if (this.editing) {
            this.editing.editing = false;
            this.editing = null;
        }
        this.state = NORMAL;
    }

    edit(uuid) {
        this.normal();
        const block_info = this.blocks_by_id[uuid];
        if (!block_info) { throw new Error('could not find block ' + uuid); }
        this.editing = block_info;
        this.state = EDITING;
        block_info.editing = true;
    }

    split_block(uuid, content, index) {
        // Split content at index
        const before = content.slice(0, index);
        const after = content.slice(index);

        // Update first block
        this.update_block_source(uuid, lodash.trim(before));

        // Create a new block with the second half, insert it, and go into
        // editing mode for it
        const block_info = this.create_block_info(lodash.trim(after));
        this.insert_after(uuid, block_info);
        this.normal();
        this.edit(block_info.id);
    }

    setup_events() {
        this.on('edit', (event, uuid) => {
            this.edit(uuid);
            this.update();
        });

        this.on('edit_to_edit', (event, edit_uuid, payload) => {
            // First switches to normal, saving change, then to edit
            const {uuid, content} = payload;
            this.update_block_source(uuid, content);
            this.edit(edit_uuid);
            this.update();
        });

        this.on('normal', (event, payload) => {
            const {uuid, content} = payload;
            this.update_block_source(uuid, content);
            this.normal();
            this.update();
        });

        this.on('split', (event, payload) => {
            const {uuid, content, index} = payload;
            this.split_block(uuid, content, index);
            this.update();
        });

        this.on('insert', (event, payload) => {
            const {after_uuid, content} = payload;
            const {$, new_html} = this.create_block(content);
            const block_info = this.new_block_info($, $('bk')[0]);
            this.insert_after(after_uuid, block_info);
            this.edit(block_info.uuid); // go directly into edit mode
            this.update();
        });
    }

    get_css() {
        // Kludgey hack, which causes duplicated CSS and unknown states for
        // when we make changes: For some reason inserting CSS directly with JS
        // was having problems
        return this.scrollobj.actions.rendercss('editor');
    }

    get_head() {
        // Also slightly kludgey: to insert contents into <head></head>
        return this.scrollobj.actions.renderhead('editor');
    }

    get_opts() {
        const css = this.scrollobj.actions.rendercss('editor');
        const blocks = this.blocks;
        return {css, blocks};
    }

    get tagname() {
        // Name of the channel this should use
        return 'document';
    }
}

module.exports = Document;
