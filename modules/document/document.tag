<block-editor>
		<textarea
				id="editing"
				uuid={opts.uuid}
				value={opts.source} />
</block-editor>

<fragment-editor>
		<span></span>
		<script>
				'use strict';
				function makeToolbar(valid_tags) {
						const html = [];
						if (!valid_tags || valid_tags.length < 1) {
								return html;
						}
						html.push('<div class="uicore-linebar red lighten-2 z-depth-3 hide-without-mouse" data-editor-mode="keep">');
						for (const tag of valid_tags) {
								if (!tag.markup || !tag.name) { continue; }
								html.push('<button class="btn waves-effect waves-white btn-flat"');
								html.push(`  data-editor-mode="keep" data-markup="${tag.markup}">`);
								html.push(tag.name);
								html.push('</button>');
						}
						html.push('</div>');
						return html;
				}

				updateContent() {
						let html = [];
						const blocks = this.opts.blocks;
						for (const block of blocks) {
								if (block.editing) {
										html.push(...[
												'<div id="editing_wrapper">',
														'<textarea id="editing" uuid="', block.id, '">',
																block.source,
														'</textarea>',
														...makeToolbar(block.options.valid_tags),
														'<div class="hidden-preview">',
																block.preview, // to get height
														'</div>',
												'</div>',
										]);
								} else {
										html.push(block.preview);
								}
						}
						this.root.innerHTML = html.join('');
						// this.root.innerHTML = opts.content;
				}

				this.on('update', () => {
						this.updateContent();
				});

				this.updateContent();
		</script>
</fragment-editor>

<document>
		<span>
				<style scoped>
						bk {
								-webkit-user-select: text;
								user-select: text;
								cursor: text;
								z-index: 1;
						}

						bk,
						textarea {
								display: block;
						}

						#editing_wrapper{
								display: block;
								position: relative;
								min-height: 1.0em;
						}

						#editing_wrapper .hidden-preview {
								visibility: hidden;
						}

						textarea {
								position: absolute;
								top: 0;
								left: 0;
								z-index: 0;
								/*min-height: 100em;*/
						}
						/* margin: 2px; */

						textarea,
						input {
								/* For Webkit, disable resize, since it is handled automatically... */
								/*box-shadow: -1px 0px 10px 0px #000, 1px 0px 10px 0px #000;*/
								/*
								box-shadow:         0px 0px 2px 1px #999;
								border-radius: 1px; 
								*/
								outline: none;
								resize: none;
								border: none;
								max-width: 100%;
								font-size: 12pt;
								text-align: left;
						}

						textarea.base_para:hover {
								background-color: #fff;
						}

						div.page-11in {
								background-color: #fafafa;
								margin: 20px;
								margin-top: 10px;
								min-height: 100vh;
								width: 11in;
								box-sizing: border-box;
								display: inline-block;
								padding: 1in;
						}
				</style>

				<!-- XXX XXX TODO: for some reason this is causing weird syntax errors -->
				<!--<raw-style css={opts.css} />-->

				<!-- Main editor -->
        <div class="z-depth-2 page-11in">
						<fragment-editor name="fragment_editor" blocks={opts.blocks} />
        </div>
		</span>

		<script>
				'use strict';
				const events = require('./js/events');

				to_edit(uuid, existing_uuid, existing_content) {
						if (existing_uuid) {
								// This is triggered if the user goes directly from one edit
								// state to another without transitioning into normal mode
								// between
								const normal_payload = {
										uuid: existing_uuid,
										content: existing_content,
								};
								this.opts.send('edit_to_edit', uuid, normal_payload);
						} else {
								this.opts.send('edit', uuid);
						}
				}

				to_split(uuid, content, index) {
						// console.log('sending split', {uuid, content, index});
						this.opts.send('split', {uuid, content, index});
				}

				to_normal(uuid, content) {
						const payload = {
								uuid: uuid,
								content: content,
						};
						this.opts.send('normal', payload);
				}

				this.on('updated', () => {
						// TODO: do diff here
						this.tags.fragment_editor.update()
						events.enhance_editor(this);
				});

				this.on('mount', () => {
						events.activate(this);
				});

				this.on('unmount', () => {
						events.deactivate(this);
				});
		</script>
</document>
