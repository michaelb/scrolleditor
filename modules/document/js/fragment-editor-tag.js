'use strict';

module.exports = function (opts) {
    window.log('fragment editor', opts);
    this.updateContent = () => {
        let html = [];
        for (const block of this.opts.blocks) {
            if (block.editing) {
                html.push([
                    '<textarea ',
                        'id="editing" ',
                        'uuid="', block.id, '">',
                        block.source,
                    '</textarea>'
                ].join(''));
            } else {
                    html.push(block.preview);
            }
        }
        this.root.innerHTML = html.join('');
        // this.root.innerHTML = opts.content;
    };

    this.on('update', () => {
        this.updateContent();
    });

    this.updateContent();
};
