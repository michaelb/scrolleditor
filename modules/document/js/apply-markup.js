// Legacy code to wrap

var _ = require('lodash');

function strip(string) {
    return string.replace(/^\s+/, '').replace(/\s+$/, '');
}

(function() {
    var fieldSelection = {
        getSelection: function() {
            var e = (this.jquery) ? this[0] : this;
            return (
                /* mozilla / dom 3.0 */
                ('selectionStart' in e && function() {
                    var l = e.selectionEnd - e.selectionStart;
                    return { start: e.selectionStart, end: e.selectionEnd, length: l, text: e.value.substr(e.selectionStart, l) };
                }) ||
                /* exploder */
                (document.selection && function() {
                    e.focus();
                    var r = document.selection.createRange();
                    if (r === null) {
                        return { start: 0, end: e.value.length, length: 0 }
                    }
                    var re = e.createTextRange();
                    var rc = re.duplicate();
                    re.moveToBookmark(r.getBookmark());
                    rc.setEndPoint('EndToStart', re);
                    return { start: rc.text.length, end: rc.text.length + r.text.length, length: r.text.length, text: r.text };
                }) ||

                /* browser not supported */
                function() { return null; }
            )();
        },
        replaceSelection: function() {
            var e = (typeof this.id == 'function') ? this.get(0) : this;
            var text = arguments[0] || '';
            return (
                /* mozilla / dom 3.0 */
                ('selectionStart' in e && function() {
                    e.value = e.value.substr(0, e.selectionStart) + text + e.value.substr(e.selectionEnd, e.value.length);
                    return this;
                }) ||

                /* exploder */
                (document.selection && function() {
                    e.focus();
                    document.selection.createRange().text = text;
                    return this;
                }) ||

                /* browser not supported */
                function() {
                    e.value += text;
                    return jQuery(e);
                }
            )();
        }
    };
    jQuery.each(fieldSelection, function(i) { jQuery.fn[i] = this; });
})();

//////////////////////////////////////////////
// Private methods that handle wrapping text around the text box
//////////////////////////////////////////////
var is_wrapped = function(text, start, end){
    var txt = strip(text);
    if (!end) end = start;
    return _.startsWith(txt, start) && 
        _.endsWith(txt, end) ;
};

var unwrapped = function(text, start, end){
    // TODO totally screws up some char sequences.
    var txt = strip(text);
    txt = strip(txt, start);
    txt = strip(txt, end);
    return txt;
};

var wrap = function(f, start, end) {
    var val = f.val();
    var selec = f.getSelection();
    if(selec.length > 0) {
        var new_text = '';
        if(is_wrapped(selec.text, start)) {
            new_text = unwrapped(selec.text, start, end);
        } else {
            new_text = start + selec.text + end;
        }
        val = val.slice(0, selec.start) + new_text
            + val.slice(selec.end, val.length);
        f.val(val);
        // Set selection to be around the text
        f.setSelection({
            start: selec.start, 
            end: selec.start + new_text.length
        });

    } else {
        // Set selection to be after
        var new_cursor = 0;
        if(val.slice(selec.start, selec.start - 
                start.length)==start) {
            // Already there, just delete that character
            val = val.slice(0, selec.start-start.length)
                + val.slice(selec.end, val.length);
        } else {
            // TODO look back to see if there's an unclosed
            // start, in which case use an end
            val = val.slice(0, selec.start) + start + 
                val.slice(selec.end, val.length);
        }
        f.val(val);
        f.setSelection(selec.start+start.length);
    }
};

function apply_markup($element, markup) {
    var split = markup.split("$");
    wrap($element, split[0], split[1]); 
}

module.exports = apply_markup;
