// TODO: make sure this works with windows...
const LINE_RE = {
    1: new RegExp(/\n\r?/g),
    2: new RegExp(/\n\r?\s*\n\r?/g)
};

module.exports.enable = function ($textarea, callback, opts) {
    opts = opts || {};
    var line_re;

    if (opts.single_line) {
        line_re = LINE_RE[1];
    } else {
        line_re = LINE_RE[2];
    }

    // Force 1 line for now:
    //line_re = LINE_RE[1];

    /*
        * Triggers a "split" event, e.g. splitting while editing at a
        * certain place
        */
    $textarea.on('keyup', function () {
        const index = $textarea.val().search(line_re);
        if (index !== -1) { callback(index); }
    });
};
