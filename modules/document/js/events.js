// XXX LEGACY

const utils = require('./utils');
const autogrow = require('./autogrow');
const autosplit = require('./autosplit');
const apply_markup = require('./apply-markup');

var last_style = null;
var last_caret = null;

function activate(document_tag) {
    var $editor = $(document_tag.root);

    $editor.on('click', 'bk', function (ev) {
        const $editing = $('#editing');
        // Trickle up to bk
        var $element = $(ev.target);
        if (!$element.is('bk')) {
            $element = $element.parents('bk');
        }

        // Alright, $element is the correct element to go into edit mode,
        // and now we'll fetch where exactly we clicked, as best we can
        // let selection = window.getSelection();
        // utils.insert_selection_caret(selection);
        var uuid;
        var content;
        if ($editing.length > 0) {
            uuid = $editing.attr('uuid');
            content = $editing.val();
        }
        document_tag.to_edit($element.attr('uuid'), uuid, content);
        last_style = utils.squirrel_away_style($element);
        last_caret = utils.squirrel_away_caret($element);
    });

    // Main catch-all "click" event
    $('body').on('click', function (ev) {
        const $target = $(ev.target);
        const $editing = $('#editing');

        /*
        // ?
        if ($target.is('.container')) {
                var cursor_y = ev.pageY;
                var $last = editor.$editor; // see if below editor
                var bottom_y = $last.offset().top + $last.outerHeight();
                if (cursor_y > bottom_y) {
                        // is at bottom!
                        return editor.edit_default();
                }
        }
        */

        if ($editing.length < 1) { return; } // not editing anything
        if ($target.is('textarea')) { return; }
        if ($target.is('#editing')) { return; } // clicked on editing element
        if ($target.attr('data-editor-mode') === 'keep') { return; }

        // ignore clicks on other editable elements, we'll catch these
        // with the other one
        if ($target.is('bk') || $target.parents('bk').length > 0) {
            return;
        }

        document_tag.to_normal($editing.attr('uuid'), $editing.val());

        // Nope, use as a "defocus" ev, get into normal mode
        //this.to_normal($editing);
        //var res = editor.start_to_normal_mode();
        //ipc.send('normal', {data: res.data, cls: res.cls});
    });
}

function deactivate(document_tag) {
    const $editor = $(document_tag.root);
    $editor.off('click', 'bk');
    $('body').off('click');
}

function enhance_editor(document_tag) {
    // Apply all the legacy editor enhancements
    const $editing = $('#editing');
    const $editing_wrapper = $('#editing_wrapper');
    if ($editing.data('enhanced')) {
        return;
    }
    $editing.focus();

    $editing_wrapper.on('click', 'button', function (ev) {
        const $elem = $(ev.target);
        apply_markup($editing, $elem.attr('data-markup'));
    });

    if (last_style !== null) {
        // we have formatting squirreled away
        $editing.css(last_style);
        last_style = null;
    }

    utils.set_caret($editing, last_caret);
    last_caret = null;

    $editing.data('enhanced', true);
    autogrow.enable($editing);
    autosplit.enable($editing, function (index) {
        document_tag.to_split(
            $editing.attr('uuid'), $editing.val(), index);
    });
}

module.exports.activate = activate;
module.exports.deactivate = deactivate;
module.exports.enhance_editor = enhance_editor;
