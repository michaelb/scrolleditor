// LEGACY code

/*

LineBar is a UI element that displays a horizontal bar between two other
elements to provide a method of inserting things

*/


var $draw_line = function (x1, y1, x2, y2, thickness, classes) {
    // distance
    var length = Math.sqrt(((x2-x1) * (x2-x1)) + ((y2-y1) * (y2-y1)));
    // center
    var cx = ((x1 + x2) / 2) - (length / 2);
    var cy = ((y1 + y2) / 2) - (thickness / 2);
    // angle
    var angle = Math.atan2((y1-y2),(x1-x2))*(180/Math.PI);
    classes = classes || 'lolnoclassforyouchumpslololol';

    // make line
    var htmlline = ["<div ",
        "style='padding:0px; margin:0px; ",
        "height:", thickness, "px;",// background-color:", color,
        "; line-height:1px; position:absolute;",
        " left:", cx, "px; top:", cy,  "px; width:", length, "px;",
        //" -moz-transform:rotate(" + angle + "deg);",
        " -webkit-transform:rotate(" + angle + "deg);",
        //" -o-transform:rotate(" + angle + "deg);",
        //" -ms-transform:rotate(" + angle + "deg);",
        " transform:rotate(" + angle + "deg);'",
        "class='uicore-line-artifact ", classes, "' ",
        "/>"].join('');
    return $(htmlline);
};

var $make_icon = function (cls) {
    return $(['<i class="', cls, 
    ' uicore-line-artifact ext-teal text-lighten-2"></i>'].join(''))
};

var get_template = function (name) {
    return window.TinyTiny(LE2.html["core/ui/html/" + name]); };

var setup_lines = function ($toolbar) {
    var $button = null;
    var $line = null;

    var show_or_hide_line = _.debounce(function () {
        // Clear all lines
        $('.uicore-line-artifact').remove();

        // lines already cleared!
        if ($line) {
            $line.remove();
            $line = null;
        }

        if ($button) {
            // get target
            var $target = $toolbar.data('target');

            // then make icons
            //var $icon1 = $make_icon('mdi-av-play-arrow'); $icon1.css('color', $button.css('background-color'));
            //var $icon2 = $make_icon('mdi-content-add-circle'); $icon2.css('color', $button.css('background-color'));
            var $icon1 = $('<div class="uicore-line-artifact" style="display: inline-block;">');
            var $icon2 = $('<div class="uicore-line-artifact" style="display: inline-block;">');

            // calculate positions
            $('body').append($icon1);
            $('body').append($icon2);
            $icon1.position({ my: "center center", at: "right center",  of: $button });
            $icon2.position({ my: "center top",    at: "center bottom", of: $target });

            var tpos = $icon1.position();
            var bpos = $icon2.position();
            $line = $draw_line(bpos.left, bpos.top, tpos.left, tpos.top, 5,
                "teal lighten-3 z-depth-1");
            $('body').append($line);
            $line.fadeIn('slow');
        }
    }, 50);

    $toolbar.on('mouseover mouseout', 'button', function (e) {
        if (e.type === 'mouseover') {
            $button = $(e.target);
            show_or_hide_line();
        } else {
            $button = null;
            show_or_hide_line();
        }
    });
};

var setup_hover_state = function ($toolbar, set_hover_state) {
    $toolbar.on('mouseover mouseout', function (e) {
        if (e.type === 'mouseover') {
            set_hover_state(true);
        } else {
            set_hover_state(false);
            //$('.uicore-line-artifact').remove();
            //opts.mouse_out();
        }
    });
};

var setup_search_button = function ($toolbar, set_hover_state) {
    var visible = false;
    $toolbar.on('click', 'button[data-search]', function (e) {
        var data = JSON.parse($(this).attr('data-search'));
        visible = !visible;
        if (visible) {
            $toolbar('.search-box').show();
        } else {
            $toolbar('.search-box').hide();
        }
        action(data);
    });
};

var linebar = function ($toolbar, options) {
    var opts = _.extend({
        set_hover_state: false,
        inserter_line: false,
        side: 'left',
        position: 'after',
    }, options);

    //var tmp = get_template('linebar');
    //var $toolbar = $(tmp(opts));
    //$('body').append($toolbar);
    //$toolbar.hide();

    if (opts.inserter_line) {
        setup_lines($toolbar);
    }

    if (opts.set_hover_state) {
        setup_hover_state($toolbar, opts.set_hover_state);
    }

    // Now we set up click events for the "args" buttons
    $toolbar.on('click', 'button[data-args]', function (e) {
        var data = JSON.parse($(this).attr('data-args'));
        action(data);
    });

    // And set up click events for any existing search buttons
    if (opts.search) {
        setup_search_button($toolbar, opts.search);
    }

    $toolbar.attr('data-posmy', opts.side === "left"
                                ? "right center" : "left center");

    var posat_prefix = opts.side;
    var posat_suffix = opts.position === "after" ? "bottom" : "center";
    $toolbar.attr('data-posat', [posat_prefix, posat_suffix].join(' '));
    return $toolbar;
};

linebar.position = function ($toolbar, $target) {
    var posmy = $toolbar.attr("data-posmy");
    var posat = $toolbar.attr("data-posat");

    $toolbar.show();
    $toolbar.position({
        //my: "right center",
        //at: "left bottom",
        my: posmy,
        at: posat,
        of: $target,
        collision: "none",
    });

    $toolbar.data('target', $target);

    /*
    $toolbar.find('.uicore-insertarrow').position({
        my: "left center",
        at: "right center",
        of: $toolbar,
        collision: "none",
    });
    */
};

module.exports = linebar;

