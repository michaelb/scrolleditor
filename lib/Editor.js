'use strict';

/*
 * Global state management, of windows and of loaded workspaces
 */
const path = require('path');
const modules = require('../modules/modules');

const WINDOW_PREFS = {
    width: 800,
    height: 600,
    autoHideMenuBar: true,
    backgroundColor: '#FFFFFFFF',
    "min-width": 200,
    "min-height": 100,
    "web-preferences": {
        "text-areas-are-resizable": false,
    },
};

// Sometimes the object names are not one to one
const EDITOR_TAG_NAMES = {
    scrollworkspace: 'workspace',
};

function exit_error(message) {
    console.error(message);
    process.exit(1);
}

class Editor {
    constructor(electron) {
        this.electron = electron;
        this.workspaces = {};
        this.windows = [];
        this.editor_cache = {};
    }

    load_window(browser_window, workspace_path) {
        this.load_workspace(workspace_path, workspace => {
            const window_info =
                this.windows.find(info => info.args[0] === workspace_path);
            window_info.workspace = workspace; // attach workspace object

            // Finally, mount the workspace in the #main element
            this.mount(window_info, workspace, '#main');
            browser_window.maximize();
        });
    }

    mount(window_info, scrollobj, selector) {
        if (!window_info) {
            // TODO: breaks with multi-window
            window_info = this.windows[0];
        }

        if (scrollobj === null) {
            throw new Error('Cannot mount null obj');
        }

        const type_info = modules.types[scrollobj.typename];
        if (!type_info) {
            throw new Error('Cannot mount unknown type ' + scrollobj);
        }

        const tagname = type_info.tagname;
        const partial_path = type_info.path;
        const editor_path = `../modules/${partial_path}`;
        const EditorClass = require(editor_path);
        const {browser_window} = window_info;

        const ipc_send = (subchannel, arg) => {
            browser_window.webContents.send(`${scrollobj.path}:${subchannel}`, arg);
        };

        // Maintain a cache
        let editor_instance;
        let html_head;
        if (this.editor_cache[scrollobj.path]) {
            editor_instance = this.editor_cache[scrollobj.path];
        } else {
            editor_instance = new EditorClass(this, scrollobj, ipc_send);
            this.editor_cache[scrollobj.path] = editor_instance;
            if (editor_instance.get_css) {
                // has global css to insert
                browser_window.webContents.insertCSS(editor_instance.get_css());
            }

            if (editor_instance.get_head) {
                // might have global <head></head> contents to insert
                html_head = editor_instance.get_head();
            }
        }

        // window_info.editors[scrollobj.typename] = editor_instance;

        const mount_payload = {
            path: `modules/${partial_path}.tag`,
            opts: JSON.stringify(editor_instance.get_opts()),
            prefix: `${scrollobj.path}:`,
            html_head: html_head,
            tagname,
            selector,
        };

        browser_window.webContents.send('mount:editor', mount_payload);
    }

    load_workspace(target, done) {
        // Import required stuff
        const ScrollWorkspace = require('libscroll/mods/workspace/ScrollWorkspace');

        // Check if target is actually a path, or else just use CWD
        let working_dir = (target.match(/\//) || target.match(/^./))
            ? path.resolve(target)
            : process.cwd();

        // XXX XXX XXX XXX
        // working_dir = path.resolve(__dirname, '..', 'spec', 'data', 'basicws');

        // Now lets get the workspace from parent dir
        ScrollWorkspace.find_parent_workspace(working_dir, workspace_dir => {
            if (workspace_dir === null) {
                exit_error('Could not find a valid Scroll workspace.');
                return;
            }

            workspace_dir = `${workspace_dir}/`;

            // Load workspace:
            ScrollWorkspace.load(workspace_dir, workspace => {
                this.workspace = workspace; // XXX
                done(workspace);
            });
        });
    }

    create_window(workspace_path) {
        const browser_window = new this.electron.BrowserWindow(WINDOW_PREFS);
        const window_id = 1000 + this.windows.length;
        const window_info = {
            window_id,
            browser_window,
            editors: {},
            args: [workspace_path],
        };
        this.windows.push(window_info);

        // Set up kick off event
        const ipc = this.electron.ipcMain;
        ipc.on('mount:ready', (event) => {
            this.load_window(browser_window, workspace_path);
        });

        ipc.on('_log', (event, payload) => {
            console.log('----', payload);
        });

        // and load the index.html of the app.
        const main_path = path.resolve(__dirname, '..', 'static', 'index.html');
        browser_window.loadURL('file://' + main_path);

        // Open the DevTools.
        // browser_window.webContents.openDevTools();

        // Emitted when the window is closed.
        browser_window.on('closed', () => {
            // Dereference the window object, usually you would store windows
            // in an array if your app supports multi windows, this is the time
            // when you should delete the corresponding element.
            this.destroy_window(window_id);
        });
    }

    destroy_window(window_id) {
        // destroys all for now
        this.windows = [];
    }
}

module.exports = Editor;
