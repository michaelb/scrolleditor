The Scroll Editor represents the graphical mode of the editor.

## Directory structure

- `lib/` - contains core 'backend' JavaScript code, run on main process
- `static/` - contains all assets (core HTML, CSS, images)
- `modules/`
    - `mixins/`
        - `cfg/`
            - some mixins for CFGs
    - `workspace/`
        - `workspace.tag` - front-end code for entire workspace (e.g. panels)
        - `workspace.js` - back-end code for workspace
    - `document/`
        - `document.tag` - front-end code for document
        - `document.js` - back-end code for document
    - `image/`


# API notes
## Editor
- Maintains a dict of open workspaces, and their corresponding windows, if
  applicable (1 workspace = 0-X browser windows, for now X should be 1 for
  simplicity)
- Can open a new window
- Bootstraps the loading of Workspace (replaces load.js) and setting up the
  communication channel between X.tag and X.js
  - Therefore, each window can have an arbitrary number of ScrollObjects open.
    Typically, that number is 2: the Workspace, and the currently edited thing,
    e.g. Document or Tag.
  - Each communication channel is thus kept separate with auto-prefixing and
    wrappers around ipc

## Editor generic concepts
- CFG generic editor:
    - Show all non-plural headers at top (e.g. [tag])
    - Plural headers show at bottom, in a well, with a +1 to add another
      instance
    - Incrementally improved as such:
        - Can have hints for syntax hilighting
        - One-by-one, parts can be swapped out for custom editors (possibly
          with the ability to "flip" into raw editor).
    - Generic "preview-able" option: this would create a new pane (if there is
      enough view space) that previews a given element, e.g. document, with
      given settings
- Parent class or interface for generic editor
    - Both `document.tag` and `generic_cfg.tag` would inherit this
    - Concepts:
        - Each has a "data=..." which contains the source data (whether it be
          markdown, or a fragment of a CFG, or maybe even binary (base64?))
        - Each has a fragment/chunk ID, that the backend Editor subclass will
          somehow map to the source document (e.g. block element number, or
          header number, or even line numbers)

## Broad notes:

### rough document.tag structure
```html
<style scoped>
    // Tag.render_css here
</style>

<!-- Render tags -->
<virtual each={tag, i for tags}>
    <virtual if={i === editing}>
        <tag-editor
            in-edit-mode={editing_index === i}
            data={tag.data}
            rendered={tag.rendered}
            onClick={activate}
            ></tag-editor>
    </virtual>
    <virtual if={i !== editing}>
        <raw html=tag.rendered></raw>
    </virtual>
</virtual>

<script>
    // Send back
    document_ipc.on('refresh', (tag_id, new_data) => {
        this.tags[tag_id] = new_data;
        this.update();
    });

    activate() {
        document_ipc.send('activating', item.id);
    }
</script>
